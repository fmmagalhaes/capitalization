import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A simple Swing-based client for the capitalization server. It has a main
 * frame window with a text field for entering strings and a textarea to see the
 * results of capitalizing them.
 */
public class CapitalizeClient {

	public static final byte[] iv = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07 };
	private BufferedReader in;
	private PrintWriter out;
	private JFrame frame = new JFrame("Capitalize Client");
	private JTextField dataField = new JTextField(40);
	private JTextArea messageArea = new JTextArea(8, 60);
	private Cipher c;
	private CipherOutputStream cos;
	private SecretKey key;
	private Socket socket;

	/**
	 * Constructs the client by laying out the GUI and registering a listener
	 * with the textfield so that pressing Enter in the listener sends the
	 * textfield contents to the server.
	 */
	public CapitalizeClient() {

		// Layout GUI
		messageArea.setEditable(false);
		frame.getContentPane().add(dataField, "North");
		frame.getContentPane().add(new JScrollPane(messageArea), "Center");

		// Add Listeners
		dataField.addActionListener(new ActionListener() {

			/**
			 * Responds to pressing the enter key in the textfield by sending
			 * the contents of the text field to the server and displaying the
			 * response from the server in the text area. If the response is "."
			 * we exit the whole application, which closes all sockets, streams
			 * and windows.
			 */
			public void actionPerformed(ActionEvent e) {

				String response;

				try {
					c.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
					cos = new CipherOutputStream(socket.getOutputStream(), c);
					out = new PrintWriter(cos, true);

					out.println(dataField.getText());
					response = in.readLine();
					if (response == null || response.equals("")) {
						System.exit(0);
					}
				} catch (Exception ex) {
					response = "Error: " + ex;
				}
				messageArea.append(response + "\n");
				dataField.selectAll();
			}
		});
	}

	public void getKey() throws IOException, InvalidKeyException, ClassNotFoundException, InvalidKeySpecException,
			NoSuchAlgorithmException {
		ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
		DESKeySpec ks = new DESKeySpec((byte[]) ois.readObject());
		SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
		key = skf.generateSecret(ks);
		// ois.close();
	}

	/**
	 * Implements the connection logic by prompting the end user for the
	 * server's IP address, connecting, setting up streams, and consuming the
	 * welcome messages from the server. The Capitalizer protocol says that the
	 * server sends three lines of text to the client immediately after
	 * establishing a connection.
	 * 
	 * @throws Exception
	 */
	public void connectToServer() throws Exception {

		// Get the server address from a dialog box.
		String serverAddress = JOptionPane.showInputDialog(frame, "Enter IP Address of the Server:",
				"Welcome to the Capitalization Program", JOptionPane.QUESTION_MESSAGE);

		// Make connection and initialize streams
		socket = new Socket(serverAddress, 9898);

		c = Cipher.getInstance("DES/CFB8/NoPadding");

		// getting key
		getKey();

		// preparing to decrypt welcoming messages
		c.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		CipherInputStream cis = new CipherInputStream(socket.getInputStream(), c);
		in = new BufferedReader(new InputStreamReader(cis));

		// Consume the initial welcoming messages from the server
		for (int i = 0; i < 3; i++) {
			messageArea.append(in.readLine() + "\n");
		}
	}

	/**
	 * Runs the client application.
	 */
	public static void main(String[] args) throws Exception {
		CapitalizeClient client = new CapitalizeClient();
		client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client.frame.pack();
		client.frame.setVisible(true);
		client.connectToServer();
	}
}
